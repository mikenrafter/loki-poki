# L.O.K.I. OS

Low-profile Omni-stable Kool Interactive Operating System

## What is it?

LOKI is a highly-optimized operating system written in YOLOL, and intended for
managing a large number of separate programs at once.

It has a low-physical profile, and relatively cheap build-cost allowing it to
be installed on almost every ship, and certainly every station.

It's built with hyper-scaling, OS-security, 24/7 uptime, and multi-user
privilege management in mind.

Convinced already? Click [here](#purchasing)/scroll down to find out how to get
your endo-hands on this!

## Feature List

Implemented:

- Easy process management. (read on)
- An ultra-stable UI.
- A list-selection API.
- A standard button configuration.  
  To allow developers to target many different form-factors.
  Tinkerers may add non-standard controls, of course.

Planned:

- Variable scoping. - To prevent name-collisions, and keep the OS secure in the
  event of social-engineering or malware - which, at a later date, may be
  transmittable remotely, in-game.
  This' a needed feature for multi-user privilege management.
- Multi-user privilege management (may useful for stations operated by companies
  at a later date)
- Process freezing. - For OS security, stability and retaining intended
  behavior + stability.

## How does it work?

LOKI uses process management techniques to ensure that your system is always
running *deepSpace.downTime == deathTime*.

### Process Management

LOKI is capable of sending a kill signal to a process, this process may
choose to ignore it, but if it does so - it will be frozen. The user has
complete control. (freeze feature pending)

### Foreground and Background Processes

LOKI can handle foreground and background processes. Foreground processes are
allowed user input, while background processes aren't, but don't run the risk
of being frozen by the OS.

LOKI uses:

- a kill signal - for ending foreground processes
- a foreground lock - to ensure that only one program is in the foreground
- a test signal - ensuring that the program is responsive, to keep the UI
  running smoothly.

### List Selection API

LOKI comes bundled with a list-selection API, providing a powerful interface for
selecting options, without adding specialized hardware.

### Plug n' play

LOKI was built with package management in mind. Installing a program is as easy
as:

1. Obtain a YOLOL chip(s)/module containing LOKI-compatible code

2. Unbolt the safety hatch located on the back of your OEM-LOKI box

3. Slide that chip into an empty slot. (a program may utilize multiple chips, or
   specialized hardware, make sure you trust what you're installing)

4. Navigate to page 0 (system-programs) on the primary screen. Select
   `SetProgs`, and wait for your normal system UI to return.  
   This will take about a second for each program slot present on the device -
   default:12
   (SetProgs program is dependent upon the IO variable scoping, and is not
   implemented yet - therefore, you must hard-code your program IDs)

5. Your program will appear on the line corresponding to the chosen
   install-slot. (6 lines/page Pg1:1-6, Pg2:7-12, etc)

6. Enjoy your easy process management.

To uninstall a program, simply remove the chip(s), instead of adding them, using
the process above.

## How do I make a program for this OS?

Reference the example program listed in this repo [greenFlash](./programs/examples/greenLight.yl).

And copy everything in the template located here
[template](./programs/examples/template.yl)

Read the comments, and enjoy!

API documentation regarding compatibility with LOKI-OS will come at a later
date, once the planned features are implemented.

**In the mean-time!** Simply develop your programs to utilize:

- `:M` - Menu-title

AND:

- `:1`, `:2`, ..., `:12` - lines 1-10 (6/page)

- `:P`, `:L` - page # and line # respectively (page 0 is reserved)

OR:

- `:A` and `:B` - 4-state buttons  
  if you need 2-state, reset their values after detection

- `:ENTR` - a 2-state button (a toggle)  
  as is with the other buttons, reset after detection, unless you don't want to

- `::`, `:;`, `:|` - displays :0-:2 respectively (1-3, put simply)

Or none of that! If your program runs in the background, and merely needs to be
toggled, you don't have to worry! The effort to transition to LOKI-OS will be
100% pain-free.

### Licensing

MIT License. We believe in progress!
